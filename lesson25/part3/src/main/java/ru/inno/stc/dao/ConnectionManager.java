package ru.inno.stc.dao;

import java.sql.Connection;

public interface ConnectionManager {

    Connection getConnection();
}
