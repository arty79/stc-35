package stc.logging.logging;

import org.apache.log4j.Logger;

public class SomeOtherClass {
    final static Logger logger = Logger.getLogger(SomeOtherClass.class);
    public SomeOtherClass(){
        logger.warn("Message WARN from SomeOtherClass");
        logger.info("Message INFO from SomeOtherClass");

    }
}
