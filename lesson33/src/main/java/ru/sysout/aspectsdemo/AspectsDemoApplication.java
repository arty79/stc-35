package ru.sysout.aspectsdemo;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import ru.sysout.aspectsdemo.service.FullNameComposer;
import ru.sysout.aspectsdemo.service.SomeService;
import ru.sysout.aspectsdemo.service.SomeServiceDouble;

@SpringBootApplication
@EnableAspectJAutoProxy
public class AspectsDemoApplication implements CommandLineRunner {
    @Autowired
    FullNameComposer composer;
    @Autowired
    SomeService service;
    @Autowired
    SomeServiceDouble service2;
    public static void main(String[] args) {
        SpringApplication.run(AspectsDemoApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        composer.composeFullName("Ivan", "Petrov");
        composer.composeFullName2("Ivan", "Petrov");
        service.someMethod();
        service2.someMethodDouble();
    }
}