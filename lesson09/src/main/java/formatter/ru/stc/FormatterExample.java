package formatter.ru.stc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;

public class FormatterExample {
    public static void main(String[] args) {
       try (BufferedWriter bw = new BufferedWriter(new FileWriter("file"))){
           Formatter formatter = new Formatter(bw);
           formatter.format("Привет из [%s], %d", "форматтера", 100);
           formatter.flush();
       } catch (IOException ex) {

       }

        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb);
        f.format("%d %d", 1, 2);
        System.out.println(sb.toString());
    }
}
