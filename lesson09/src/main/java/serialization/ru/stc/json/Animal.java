package serialization.ru.stc.json;

import javax.xml.bind.annotation.XmlAttribute;

public class Animal {
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
