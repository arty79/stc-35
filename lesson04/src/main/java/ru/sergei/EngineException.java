package ru.sergei;

public class EngineException extends Exception {
    public EngineException(String message) {
        super(message);
    }
}
