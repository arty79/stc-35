package file_path;

import java.io.File;
import java.io.IOException;

public class FileDemo {
    public static void main(String[] args) {
        File file = new File("file1.txt");
        try {
            System.out.println("создали файл [" + file.getAbsolutePath() +
                    "] " + file.createNewFile());
        } catch (IOException ex) {
            System.out.println("Исключение " + ex.getMessage());
        }
        getInfo(new File("file_path/testData/fileA.txt"));
        getInfo(file);
        getInfo(new File(".\\..\\.."));
    }

    private static void getInfo(File fileA) {
        try {
            System.out.println("Каноничный путь:" + fileA.getCanonicalPath());
            System.out.println("Абсолютный путь:" + fileA.getAbsolutePath());
            System.out.println("Это директория?" + fileA.isDirectory());
            System.out.println("Это файл?" + fileA.isFile());
            System.out.println("Это абсолютный путь?" + fileA.isAbsolute());

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
