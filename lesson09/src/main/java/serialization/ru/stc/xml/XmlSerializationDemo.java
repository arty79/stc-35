package serialization.ru.stc.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

public class XmlSerializationDemo {
    public static void main(String[] args) throws JAXBException {
        Cat cat = new Cat("Барсик", 4,
                new Toy[]{new Toy("Мячик", "синий"), new Toy("Мышка", "серый")});
        Dog dog = new Dog("Тузик", "косточка");


        JAXBContext context = JAXBContext.newInstance(Cat.class, Dog.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        marshaller.marshal(dog, System.out);
        marshaller.marshal(cat, System.out);

        try(OutputStream fos = new FileOutputStream("cat.xml")) {
            marshaller.marshal(cat, fos);
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println(cat.toString());
        Unmarshaller unmarshaller = context.createUnmarshaller();
        try(InputStream is = new FileInputStream("cat.xml")) {
            Cat cat2 = (Cat) unmarshaller.unmarshal(is);
            System.out.println(cat2.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
