package ru.sergei;

public class EngineNotStartedException extends EngineException {

    public EngineNotStartedException(String message) {
        super(message);
    }
}
