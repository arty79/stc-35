CREATE OR REPLACE FUNCTION shipment_insert_trigger() RETURNS TRIGGER AS $$
BEGIN IF ( NEW.location_id >= 1 AND NEW.location_id <= 10)
THEN INSERT INTO shipment_loc1 VALUES (NEW.*);
ELSIF ( NEW.location_id >= 11 AND NEW.location_id <= 20)
THEN INSERT INTO shipment_loc2 VALUES (NEW.*);
ELSE RAISE EXCEPTION 'Date out of range.  Fix the shipment_insert_trigger() function!';
END IF;
RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_shipment_trigger
BEFORE INSERT ON shipment
FOR EACH ROW EXECUTE PROCEDURE shipment_insert_trigger();