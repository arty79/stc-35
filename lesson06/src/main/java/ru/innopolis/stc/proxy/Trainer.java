package ru.innopolis.stc.proxy;

public interface Trainer {
    public void teach();

    public void eat();

    public void talk();
}
