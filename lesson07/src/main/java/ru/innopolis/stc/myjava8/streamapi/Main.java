package ru.innopolis.stc.myjava8.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Gamer gamer1 = new Gamer(343, true, (short) 100, "Randie");
        Gamer gamer2 = new Gamer(342, false, (short) 150, "Dannie");
        Gamer gamer3 = new Gamer(244, false, (short) 200, "Cole");
        Gamer gamer4 = new Gamer(243, true, (short) 250, "Brandon");
        Gamer gamer5 = new Gamer(224, false, (short) 290, "Holey");
        Gamer gamer6 = new Gamer(143, true, (short) 280, "Nick");
        Gamer gamer7 = new Gamer(311, true, (short) 310, "Mary");
        ArrayList<Gamer> gamerList = new ArrayList<>();
        gamerList.add(gamer1);
        gamerList.add(gamer2);
        gamerList.add(gamer3);
        gamerList.add(gamer4);
        gamerList.add(gamer5);
        gamerList.add(gamer6);
        gamerList.add(gamer7);

        printAll(gamerList);



/*        printAll(collect(gamerList.stream().filter(
                (g) -> g.isProf()
        ).toArray()));*/
    }

    private static void printAll(ArrayList<Gamer> list){
        list.forEach((gamer) -> System.out.println(gamer.toString()));
    }

    private static List<Gamer> collect (Stream<Gamer> stream) {
        List <Gamer> gamers = new ArrayList<>();
        stream.forEach((g) -> gamers.add(g));
        return gamers;
    }


}
