package com.inno.stc;

public class OutOfMemoryError {

    public static void main(String args[]) throws InterruptedException {
        OutOfMemoryError ome = new OutOfMemoryError();
        ome.generateMyIntArray(1,50);
    }

    public void generateMyIntArray(int start, int end) throws InterruptedException {
        int multiplier = 100;
        for(int i = 1; i < end; i++) {
            Thread.sleep(10000);
            System.out.println("Round " + i + " Free Memory: " + Runtime.getRuntime().freeMemory());
            int[] myIntList = new int[multiplier];
            for(int j= i; j > 1; j--){
                myIntList[j] = i;
            }
            multiplier = multiplier * 10;
        }
    }
}