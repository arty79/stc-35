package ru.inno.stc.service;

import ru.inno.stc.entity.Person;

import java.io.IOException;
import java.util.List;

public interface PersonService {

    List<Person> getList();

    boolean addPerson(String name, String birth) throws IOException;
}
