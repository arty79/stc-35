package ru.innopolis.stc.myjava8.lambda.start;

@FunctionalInterface
public interface TaxCalculator {
    double calculateTax(int summ);
}
