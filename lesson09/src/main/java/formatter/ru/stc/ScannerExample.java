package formatter.ru.stc;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) throws IOException {
        try(Scanner scanner = new Scanner(new File("data"))){
            scanner.useDelimiter("~|\r\n|\n");
            int error = 0;
            while (scanner.hasNext()) {
                String s = scanner.next();
                if(!scanner.hasNextInt()) {
                    error++;
                    continue;
                }
                Integer i = scanner.nextInt();
                if(!scanner.hasNextFloat()) {
                    error++;
                    continue;
                }
                Float f = scanner.nextFloat();
                String v = String.format("[%-20s][%05d][%+5.2f]\n", s, i, f);
                System.out.print(v);
            }
            System.out.printf("Ошибочных строк %d", error);
        }
    }
}
