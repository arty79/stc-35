package future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class App2 {
    public static void main(String[] args) throws InterruptedException {
        SquareCalculator squareCalculator = new SquareCalculator();
        try {
            System.out.println(squareCalculator.calculate(5).get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Future<Integer> future = new SquareCalculator().calculate(10);

        while(!future.isDone()) {
            System.out.println("Calculating...");
            Thread.sleep(300);
        }

        try {
            Integer result = future.get();
            System.out.println(result);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
