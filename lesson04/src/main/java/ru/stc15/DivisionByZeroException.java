package ru.stc15;

public class DivisionByZeroException extends Throwable {

    @Override
    public String getMessage() {
        return "Произошло деление на 0";
    }
}
