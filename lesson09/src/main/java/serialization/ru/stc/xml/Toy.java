package serialization.ru.stc.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "toy")
public class Toy {
    private String name;
    private String color;

    public Toy(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Toy() {
    }

    @XmlElement
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("игрушка [имя:%s, цвет:%s]", name, color);
    }
}
