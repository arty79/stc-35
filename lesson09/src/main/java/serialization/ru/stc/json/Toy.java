package serialization.ru.stc.json;


import com.fasterxml.jackson.annotation.JsonInclude;

public class Toy {
    private String name;
    private String color;

    public Toy(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Toy() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("игрушка [имя:%s, цвет:%s]", name, color);
    }
}
