package file_path;

import java.io.File;

public class FileDemo2 {
    public static void main(String[] args) {
        File dir = new File("file_path/testData/a/b");
        System.out.println("создаем директории " + dir.getPath() + " " +
                dir.mkdirs());
        File dirB = new File("file_path/testData/b");
        dirB.deleteOnExit();
        System.out.println("создаем директории " + dirB.getPath() + " " +
                dirB.mkdir());

        File file = new File("file_path/testData/c.txt");
        if (file.exists()) {
            System.out.println("размер файла " + file.getName() + "=" + file.length());
        } else {
            System.out.println("файла " + file.getName() + " не существует");
        }

        listDirectory(new File("file_path/testData/"));
        System.out.println("удаляем " + dir.getPath() + " " + dir.delete());

    }

    private static void listDirectory(File files) {
        System.out.println("Содержимое каталога " + files.getPath());
        for(File file : files.listFiles()){
            if(file.isDirectory())
                System.out.print("!");
            if(file.isFile())
                System.out.print("*");

            System.out.println(file.getName());
        }
    }
}
