package serialization.ru.stc;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class ReadDataFromInternet {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://api.icndb.com/jokes/random");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader)){
                String line;
                while ((line = br.readLine())!=null){
                    System.out.println(line);
                }
            } catch (IOException e) {
            }
        }catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
