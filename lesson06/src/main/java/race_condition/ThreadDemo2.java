package race_condition;

import java.util.ArrayList;
import java.util.List;

public class ThreadDemo2 {

    public static void main(String[] args) throws InterruptedException {
        demo2();
    }

    private static void demo2() throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        Counter counter = new Counter();
        for (int i = 0; i < 10; i++) {
            Thread thread = new ResThread(counter);
            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads) {
            thread.join();
        }

        System.out.println("Count = " + counter.getCount());
    }

}
