package ru.innopolis.stc.myjava8.streamapi;

public class Gamer {
    private long weight;
    private boolean isProf;
    private short level;
    private String nickName;

    public Gamer(long weight, boolean isProf, short level, String nickName) {
        this.weight = weight;
        this.isProf = isProf;
        this.level = level;
        this.nickName = nickName;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public boolean isProf() {
        return isProf;
    }

    public void setProf(boolean prof) {
        isProf = prof;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "Gamer{" +
                "weight=" + weight +
                ", isProf=" + isProf +
                ", level=" + level +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}
