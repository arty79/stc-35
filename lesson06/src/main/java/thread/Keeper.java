package thread;

public class Keeper {
    private static Data instance = null;

    public Data getData() {
        if(instance == null) {
            synchronized(this) {
                if(instance == null) {
                    instance = new Data();
                }
            }
        }
        return instance;
    }
}