package formatter.ru.stc;

import java.util.Objects;

public class FormatExample {
    public static void main(String[] args) {
        String name = "Вова";
        int age = 19;
        boolean male = true;
        System.out.printf("Имя: %s, Возраст: %d, Мальчик: %b\n", name, age, male);
        Integer ageBoxed = age;
        System.out.printf("%d %b %d\n", ageBoxed, ageBoxed==age, age);
        Integer ageBoxed2 = new Integer(age);
        System.out.printf("%d %b %d\n", ageBoxed, ageBoxed==ageBoxed2, ageBoxed2);
        System.out.printf("%d %b %d\n", ageBoxed, ageBoxed.equals(ageBoxed2), ageBoxed2);

        ageBoxed = null;
        ageBoxed2 = null;
        System.out.printf("%d %b %d\n", ageBoxed, Objects.equals(ageBoxed, ageBoxed2), ageBoxed2);
        Integer boxed[] = new Integer[30];
        for(int i = 0; i < 30; i++) {
                boxed[i] = i + 100;
        }

        for(int i = 0; i < 30; i++) {
            Integer boxedI = i + 100;
            System.out.printf("%d %b %d\n", boxed[i], boxed[i]==boxedI, boxedI);
        }

        Boolean value = new Boolean(true);
        Boolean value2 = Boolean.parseBoolean("true");
        Float.parseFloat("10.100");

    }
}
