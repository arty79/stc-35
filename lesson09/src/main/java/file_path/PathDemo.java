package file_path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathDemo {
    public static void main(String[] args) {
        pathInfo(Paths.get("file_path/testData"));
        Path path = Paths.get("C:\\Users\\STC\\IdeaProjects\\vlad\\FilesAndPaths\\file_path.testData");
        pathInfo(path);
        System.out.println("корень " + path.getRoot().toString());

        Path path1 = Paths.get("C:\\Users\\STC\\IdeaProjects\\");
        pathInfo(path1.resolve("..\\vlad\\FilesAndPaths\\file_path.testData"));
    }

    private static void pathInfo(Path path) {
        System.out.println("путь " + path.toString() + " абсолютный " +
                path.isAbsolute());
        System.out.println("канонически путь " + path.normalize().toString());
    }
}
