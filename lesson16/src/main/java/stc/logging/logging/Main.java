package stc.logging.logging;
import org.apache.log4j.Logger;

public class Main {
    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        logger.debug("debug-message"); // all
        logger.info("info-message"); // except debug
        logger.warn("warn-message"); // except debug and info
        logger.error("error-message"); // except debug, info and warn
        logger.fatal("fatal-message"); // only fatal
        try {
            throw new Exception("test exception");
        } catch (Exception e) {
            logger.error( e.getMessage());
        }

        SomeOtherClass someOtherClass = new SomeOtherClass();
    }
}
