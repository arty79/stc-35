package ru.innopolis.stc.myjava8.lambda.start;

@FunctionalInterface
public interface TaxCalculator2 {
    double calculateTax (int summ);

    default double calculateTax2 (int summ){
        return 0;
    }
}
