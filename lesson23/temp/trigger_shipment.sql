CREATE OR REPLACE FUNCTION shipment_insert_trigger() RETURNS TRIGGER AS $$
DECLARE
id INT;
BEGIN
IF (TG_OP = 'INSERT') AND ( NEW.quantity > 10) AND ( NEW.quantity < 20)
THEN
id = NEW.id;
INSERT INTO shipment VALUES (NEW.*);
ELSE RAISE EXCEPTION 'Date out of range.  Fix the shipment_insert_trigger() function!';
END IF;
RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_shipment_trigger
BEFORE INSERT ON contract
FOR EACH ROW EXECUTE PROCEDURE shipment_insert_trigger();
