package ru.sysout.aspectsdemo.service;

import org.springframework.stereotype.Service;
import ru.sysout.aspectsdemo.LogExecutionTime;

@Service
public class SomeServiceDouble {
    @LogExecutionTime
    public void someMethodDouble() throws InterruptedException {
        System.out.println("Help!");
        Thread.sleep(2000);
    }
}
