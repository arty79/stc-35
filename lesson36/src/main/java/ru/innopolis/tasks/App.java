package ru.innopolis.tasks;

import java.util.stream.IntStream;

public class App {
    public static void main(String[] args) {
        fuzzbuzz();
        System.out.println();
        fuzzbuzzBeforeJava8(100);
        System.out.println();

    }

    private static void fuzzbuzzBeforeJava8(int num) {
        for (int i = 1; i <= num; i++) {
            if (((i % 5) == 0) && ((i % 7) == 0)) // Is it a multiple of 5 & 7?
                System.out.print(" fizzbuzz ");
            else if ((i % 5) == 0)// Is it a multiple of 5?
                System.out.print(" fizz ");
            else if ((i % 7) == 0)// Is it a multiple of 7?
                System.out.print(" buzz ");
            else System.out.print(i); // Not a multiple of 5 or 7
        }
    }

    private static void fuzzbuzz() {
        IntStream.rangeClosed(1, 100)
                .mapToObj(i -> i % 5 == 0 ? (i % 7 == 0 ? " FizzBuzz " : " Fizz ") : (i % 7 == 0 ? " Buzz " : i))
                .forEach(System.out::print);
    }
}
