package ru.src15;

import java.io.CharConversionException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class InheritanceDemo {
    public static void main(String[] args) {
        Root root = new Root();
        try {
            root.doSome();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Root {
    void doSome() throws IOException {
    }
}

class Child extends Root{
    @Override
    void doSome() {
    }
}