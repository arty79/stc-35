package ru.sergei;

public class TreeException extends RuntimeException {
    public TreeException(String message) {
        super(message);
    }
}
