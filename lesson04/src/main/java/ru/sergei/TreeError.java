package ru.sergei;

public class TreeError extends Error {
    public TreeError(String message) {
        super(message);
    }
}
