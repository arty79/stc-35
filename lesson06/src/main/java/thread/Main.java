package thread;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class Main {
    public static void main(String[] args) {

        //trouble 1
        String s1 = "/usr/tmp";
        String s2 = s1.substring(4);   // contains "/tmp"
    }

    public class Trouble2 {
        Map configOptions;
        char[] configText;
        volatile boolean initialized = false;

        // In thread A

        //configOptions = new HashMap();
        //configText = readConfigFile(fileName);
        //processConfigOptions(configText, configOptions);
        //initialized = true;


        // In thread B

        //while (!initialized)
            //sleep();
        // use configOptions
    }
}
