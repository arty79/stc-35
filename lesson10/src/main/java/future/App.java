package future;

public class App {
    public static void main(String[] args) throws InterruptedException {
        MyClass myClass = new MyClass();
        Thread t = new Thread(myClass);
        t.setName("MyThread");
        t.start();

        System.out.println(Thread.currentThread().getName());
        //Thread.currentThread().sleep(1000);
        System.out.println("finish");
    }
}
