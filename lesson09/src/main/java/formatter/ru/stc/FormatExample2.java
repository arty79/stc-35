package formatter.ru.stc;

import java.util.Locale;
import java.util.Scanner;

public class FormatExample2 {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        Student[] students = new Student[]{
                new Student("Маша", 25, false, 4.99999f),
                new Student("Петя", 26, true, 3.789f),
                new Student("Витя", 24, true, 4.2f)
        };
        for (Student student: students) {
            System.out.printf("[%-30s]\n", student);
        }
        try {
            Student s = readStudent();

            System.out.println(s);
        }catch (RuntimeException re) {
            //TODO Обработать исключение
        }
    }

    static Student readStudent() {
        Student result = new Student();
        System.out.print("Введите имя:");
        if(!sc.hasNext()) {
            throw new RuntimeException("Не было введено имя!");
        }
        result.setName(sc.nextLine());

        System.out.print("Введите возаст:");
        if(!sc.hasNextInt()) {
            throw new RuntimeException("Не было введено имя!");
        }
        result.setAge(sc.nextInt());
        //....
        return result;
    }
    static class Student {
        String name;
        int age;
        boolean male;
        float rating;

        public String toString() {
            return String.format(Locale.US, "%s, %d, %b, %.3f", name, age, male, rating);
        }
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public boolean isMale() {
            return male;
        }

        public void setMale(boolean male) {
            this.male = male;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }
        public Student() {

        }
        public Student(String name, int age, boolean male, float rating) {
            this.name = name;
            this.age = age;
            this.male = male;
            this.rating = rating;
        }

    }
}
