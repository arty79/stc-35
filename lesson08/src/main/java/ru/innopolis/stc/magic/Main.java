package ru.innopolis.stc.magic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException,
            IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, IOException {
        ClassLoader parentClassLoader = BadMagic.class.getClassLoader();
        Class magicClass = parentClassLoader
                .loadClass("ru.innopolis.stc.magic.BadMagic");
        BadMagic badMagic = (BadMagic) magicClass.newInstance();
        badMagic.cast();

        KindMagicClassLoader kindMagicClassLoader;
        for (; ; ) {
            kindMagicClassLoader =
                    new KindMagicClassLoader(parentClassLoader);
            Class kindMagicClass = kindMagicClassLoader
                    .loadClass("ru.innopolis.stc.magic.BadMagic");
            Object magic = kindMagicClass.newInstance();
            //kindMagicClass.cast(magic).cast();
            kindMagicClass.getMethod("cast")
                    .invoke(kindMagicClass.newInstance(), null);
            new BufferedReader(
                    new InputStreamReader(System.in)).readLine();
        }
    }
}
