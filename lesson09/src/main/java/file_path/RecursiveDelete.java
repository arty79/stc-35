package file_path;

import java.io.File;
import java.io.IOException;

public class RecursiveDelete {
    public static void main(String[] args) {
        createTestData();
        System.out.println(delete(new File("testData2"), 0));
    }
    private static int delete(File root, int depth) {
        int result = depth;
        if (root != null && root.exists()) {
            if(root.isDirectory()) {
                for (File file : root.listFiles()) {
                    result = Math.max(result, delete(file, depth + 1));
                }
            }
            root.delete();
        }
        return result;
    }
    private static void createTestData() {
        try {
            new File("testData2/a/b/c/d").mkdirs();
            new File("testData2/a/c/a/a").mkdirs();
            new File("testData2/b/a/a").mkdirs();
            new File("testData2/b/c/b").mkdirs();
            new File("testData2/a/1.txt").createNewFile();
            new File("testData2/b/2.txt").createNewFile();
            new File("testData2/b/a/3.txt").createNewFile();
            new File("testData2/b/a/c/d/4.txt").createNewFile();
            new File("testData2/a/b/c/d/5.txt").createNewFile();
        } catch (Exception e){
        }
    }

}
