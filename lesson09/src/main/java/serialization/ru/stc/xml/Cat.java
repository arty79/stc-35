package serialization.ru.stc.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Animal")
public class Cat extends Animal {
    private Integer age;
    private Toy[] toys;

    public Cat() {

    }

    public Cat(String name, Integer age, Toy[] toy) {
        super.name = name;
        this.age = age;
        this.toys = toy;
    }

    @XmlElement
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @XmlElementWrapper
    @XmlElement(name = "toy")
    public Toy[] getToys() {
        return toys;
    }

    public void setToys(Toy[] toys) {
        this.toys = toys;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (Toy toy : toys) {
            sb.append(toy.toString());
        }
        return String.format("Кошка [ имя:%s, возраст:%d, игрушки:%s]",
                name, age, sb);
    }
}
