package serialization.ru.stc.xml;

import javax.xml.bind.annotation.XmlAttribute;

public class Animal {
    protected String name;

    @XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
