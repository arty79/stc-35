package blocks;

public class Cat extends Animal implements Running, Mieowing, Eating {
    public Cat(String name, Integer nLegs) {
        super(name, nLegs);
    }

    public void eat() {
        System.out.println("Cat eats");
    }

    public void mieow() {
        System.out.println("Mieow");
    }

    public void run() {
        System.out.println("Cat runs");
    }
}
