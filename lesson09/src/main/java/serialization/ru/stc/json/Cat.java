package serialization.ru.stc.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cat extends Animal {
    private Integer age;
    private Toy[] toys;
    String secret;

    public Cat() {

    }

    public Cat(String name, Integer age, Toy[] toy) {
        super.name = name;
        this.age = age;
        this.toys = toy;
    }

    @JsonProperty(value = "catAge")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Toy[] getToys() {
        return toys;
    }

    public void setToys(Toy[] toys) {
        this.toys = toys;
    }

    @JsonIgnore
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (Toy toy : toys) {
            sb.append(toy.toString());
        }
        return String.format("Кошка [ имя:%s, возраст:%d, игрушки:%s]",
                name, age, sb);
    }
}
