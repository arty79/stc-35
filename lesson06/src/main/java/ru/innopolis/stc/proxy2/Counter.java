package ru.innopolis.stc.proxy2;

public interface Counter {
    public void count();
}
