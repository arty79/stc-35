package formatter.ru.stc;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipStreamExample {
    public static void main(String[] args) throws IOException {
        try(FileOutputStream f = new FileOutputStream("data.zip");
            ZipOutputStream zos = new ZipOutputStream(f)) {
            zos.putNextEntry(new ZipEntry("file.txt"));
            zos.write("hello world!\ni'm second string!".getBytes());
            zos.closeEntry();
            zos.putNextEntry(new ZipEntry("file2.txt"));
            zos.write("bye bye!".getBytes());
            zos.closeEntry();
        }

        try (FileInputStream f = new FileInputStream("data.zip");
             ZipInputStream zis = new ZipInputStream(f)){
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null){
                System.out.println(entry.getName());
                BufferedReader r = new BufferedReader(new InputStreamReader(zis));
                String line;
                while ((line = r.readLine()) != null) {
                    System.out.println(line);
                }
            }
        }
    }
}
