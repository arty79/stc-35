package file_path;

public class Recursion {
    public static void main(String[] args) {
        System.out.println("факториал от 6 = " + recFact(6));
        System.out.println("факториал от 6 = " + iterFact(6));
        for (int i = 1; i < 8; i++) {
            System.out.print(fib(i)+" ");
        }
    }

    public static long recFact(int n) {
        return (n == 1) ?
                1 :
                n * recFact(n - 1);
    }

    public static long iterFact(int n) {
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

    public static long fib(long n) {
        if(n < 3) {
            return 1;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }
}
