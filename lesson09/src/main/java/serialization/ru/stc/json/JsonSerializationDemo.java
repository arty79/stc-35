package serialization.ru.stc.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import serialization.ru.stc.xml.Cat;
import serialization.ru.stc.xml.Dog;
import serialization.ru.stc.xml.Toy;

import java.io.File;

public class JsonSerializationDemo {
    public static void main(String[] args) {
        Cat cat = new Cat("Барсик", 4,
                new Toy[]{
                        new Toy("Рыбка", null),
                        new Toy("Мячик", "синий"), new Toy("Мышка", "серый")});
        Dog dog = new Dog("Тузик", "косточка");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        try {
            objectMapper.writeValue(new File("cat.json"), cat);
            objectMapper.writeValue(new File("dog.json"), dog);
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(cat.toString());
        System.out.println(dog.toString());

    }
}
